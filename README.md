# Quantum MErge

This is a repo for jupyter notebooks for introducing quantum mechanics to students who have or are learning quantum computing. 

## Description

This repo is for people from both quantum mechanics and computer science background to contribute for both the disciplines on expanding knowledge of both areas. Primarily it was focused on showing CS students a way into quantum mechanics, but this can be used for the other way around.

## Video 

This [video](https://drive.google.com/file/d/1soIO9k9TKl-onooYwm8VvhIbQsW3sbx2/view?usp=sharing) will tell you about the first notebooks that were made just as a basic walk through.

## Installation

Use Vscode or jupyter notebook or even google colab to open it online.

## Support
Right now the best help is asking for the question on stack overflow or opening an issue on this repo.

## Roadmap
We plan to expand this notebook in the direction of quantum mechanics with something like quantum Composer for visualzations.  You can check it out [here](https://www.quatomic.com/composer/).

## Contributing

Add the notebooks for the issues first before adding your own topic. Ensure that you have read on that topic from authentic resources before you start a notebook. cite the text you refered it from along with page number to ensure that the student can verify and further study where nessecarry. 

## Authors and acknowledgment
The initial authors of the project are Muhammad Usaid and Osama Arif Lone

## License
Apache Open source license

## Project status

This project has taken off and we are requesting contributions.
